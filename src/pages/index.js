import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './index.module.css';
import HomepageFeatures from '../components/HomepageFeatures';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className="container">
        <h1 className="hero__title">Xperience Studio</h1>
        <p className="hero__subtitle">DOCUMENTATION</p>
        
      </div>
    </header>
  );
}

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`Documentation | Xperience Studio`}
      description="">
      <HomepageHeader />
      <main>
      <div className="text--center padding-horiz--md">
        <br></br>
        <h3>Links to follow</h3>
      </div>
      
        <HomepageFeatures />
      </main>
    </Layout>
  );
}

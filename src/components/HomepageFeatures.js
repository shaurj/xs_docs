import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {},
  {
    title: 'Xperience Studio',
    //img: `../../static/img/SX_LOGO.png`,
    link:"http://193.122.128.117:9001/",
    description: (
      <>
        Xperience Studio is a super simple and low code envionmnet 
        for easy frontend developement and for UI and backend integration.
      </>
    ),
  },
  {
    title: 'Rapidly',
    //img: `../../static/img/Rapidly.jpg`,
    link:"http://193.122.128.117:8002/",
    description: (
      <>
        Rapidly is a one stop platform for API Developers which helps to create, 
        publish and share API's quickly and easily. 
      </>
    ),
  },
  {}
];

function Feature({img, title, description,link}) {
  return (
    <div className={clsx('col col--3')}>
      <div className="text--center padding-horiz--md">
         <br></br>
         <a href={link} target="_blank"> <h2>{title}</h2> </a>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}

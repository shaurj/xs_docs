---
sidebar_position: 5
---

### Properties and Parameters

| Property Type (Logical/Visual) | Name                      | Mandatory/Optional | Constraint | ReadOnly | Sample Value           | Default Value          | Description                                                       |
| ------------------------------ | ------------------------- | ------------------ | ---------- | -------- | ---------------------- | ---------------------- | ----------------------------------------------------------------- |
| logical                        | name                      | Mandatory          |            |          | Purchasing Workbench   |                        | name identifier of the dashboard                                  |
| logical                        | desc                      | Optional           |            |          | PWB                    |                        | short description of the dashboard                                |
| visual                         | theme - background\_color | Optional           |            |          | rgba(255, 255, 255, 1) | rgba(255, 255, 255, 1) | dashboard background color                                        |
| visual                         | theme -accent color       | Optional           |            |          | rgba(3, 169, 244, 1)   | rgba(3, 169, 244, 1)   | colors that are used for emphasis in a color scheme               |
| visual                         | theme - text\_color       | Optional           |            |          | rgba(0, 0, 0, 0.87)    | rgba(0, 0, 0, 0.87)    | color of text                                                     |
| visual                         | theme - border\_color     | Optional           |            |          | rgb(255,253,252)       | rgb(255,253,252)       | border color                                                      |
| visual                         | theme - border\_radius    | Optional           |            |          | 4px                    | 4px                    | CSS property rounds the corners of an element's outer border edge |
| visual                         | theme - text\_family      | Optional           |            |          | Roboto Medium          | Roboto Medium          | font text family                                                  |
| visual                         | theme - text\_size        | Optional           |            |          | 12px                   | 12px                   | font text size                                                    |
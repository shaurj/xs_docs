---
sidebar_position: 3
---

In Xperience Studio the properties are divided in 3 parts:

- Logical
- Runtime
- Visual

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/properties in xs.mp4" type="video/mp4"/>
  </video>
</figure>
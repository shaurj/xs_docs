---
sidebar_position: 8
id: DataSource
title : DataSource
---
### Data Source using Open API

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/DataSource.mp4" type="video/mp4"/>
  </video>
</figure>

---
### Data Source using REST

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/DatasrcRest.mp4" type="video/mp4"/>
  </video>
</figure>

---

### Properties And Parameters


| Property Type (Logical/Visual) | Name              | Sub Property Name | Mandatory/Optional | Constraint       | ReadOnly | Sample Value                                                                                                                                                                                                                                 | Default Value | Description                                                                 |
| ------------------------------ | ----------------- | ----------------- | ------------------ | ---------------- | -------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------- | --------------------------------------------------------------------------- |
| logical                        | name              |                   | Mandatory          |                  |          | DataSource1                                                                                                                                                                                                                                  | null          | menuBar Name                                                                |
| logical                        | operation         |                   | Mandatory          |                  | TRUE     | get                                                                                                                                                                                                                                          |               | api operation name                                                          |
| logical                        | api               |                   | Mandatory          |                  |          | [http://193.122.128.117:8182/ords/apps/open-api-catalog/](http://193.122.128.117:8182/ords/apps/open-api-catalog/)                                                                                                                           |               | api gateway url                                                             |
| logical                        | header            |                   | Optional           |                  |          | {<br/>    'Content-Type': 'application/json',<br/>    'X\_USER\_NAME': 'STAKKE',<br/>    'X\_RESP\_ID': 'PO\_MGR\_DIGITAL\_PLATFORM',<br/>  }                                                                                                    |               | header api information                                                      |
| logical                        | Auth Type         |                   | Optional           |                  |          | "security": {<br/>      "security": "PASSTHROUGH",<br/>      "cred": {<br/>          "clientId": "",<br/>          "clientSecret": "",<br/>          "tokenLink": "",<br/>          "username": "",<br/>          "password": ""<br/>      }<br/>  }, |               | auth Info of the endpoint                                                   |
| logical                        | bodyParamsList    |                   | Optional           |                  | TRUE     |  \["SEGMENT1","approval\_status","supplier" \]                                                                                                                                                                                               |               | available body Parameters list for the api  inherited from catalog registry |
| logical                        | queryParamsList   |                   | Optional           |                  | TRUE     |  \["SEGMENT1","approval\_status","supplier" \]                                                                                                                                                                                               |               | available query Parameters list for the api inherited from catalog registry |
| logical                        | responseStructure |                   | Optional           |                  | TRUE     | "{<br/>    ""SEGMENT1"": ""4590""<br/>}"<br/>                                                                                                                                                                                                   |               | api response structure                                                      |
| logical                        | payloadStructure  |                   | Optional           | operation = post |          | {<br/>    "SEGMENT1": "4590"<br/>}                                                                                                                                                                                                             |               | post api input payload structure                                            |
| runtime                        | body              |                   | Optional           |                  |          |                                                                                                                                                                                                                                              |               |                                                                             |
| runtime                        | queryParams       |                   | Optional           |                  |          | "p\_in\_count\_only": "Y"                                                                                                                                                                                                                    |               | api query Param                                                             |
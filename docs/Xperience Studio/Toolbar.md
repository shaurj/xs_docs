---
sidebar_position: 13
---
Toolbar helps user to create buttons which can be use in many useful ways.

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/toolbar-button.mp4" type="video/mp4"/>
  </video>
</figure>

---
### Properties and Parameters

| Property Type (Logical/Visual) | Name        | Sub Property Name | Mandatory/Optional | Constraint | ReadOnly | Sample Value          | Default Value | Description                       |
| ------------------------------ | ----------- | ----------------- | ------------------ | ---------- | -------- | --------------------- | ------------- | --------------------------------- |
| logical                        | name        |                   | Mandatory          |            |          | Create PO             | null          | button Name                       |
| logical                        | toolbarType |                   | Mandatory          |            |          | action/draggable/menu | action        | button type                       |
| visual                         | width       |                   | Mandatory          |            |          | 450px                 | 300px         | defines the width of the toolbar  |
| visual                         | Height      |                   | Mandatory          |            |          | 450px                 | 300px         | defines the height of the toolbar |
| visual                         | x           |                   | Mandatory          |            |          | 20                    | 0             | x coordinate of the toolbar       |
| visual                         | y           |                   | Mandatory          |            |          | 40                    | 0             | y coordinate of the toolbar       |
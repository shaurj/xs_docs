---
sidebar_position: 11
id: LOV
title: LOV
---

### Basics

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/LOV.mp4" type="video/mp4"/>
  </video>
</figure>

---

### Properties and Parameters

| Property Type (Logical/Visual) | Name           | Sub Property Name | Mandatory/Optional | Constraint | ReadOnly | Sample Value                                       | Default Value    | Description                                                         |
| ------------------------------ | -------------- | ----------------- | ------------------ | ---------- | -------- | -------------------------------------------------- | ---------------- | ------------------------------------------------------------------- |
| logical                        | name           |                   | Mandatory          |            |          | operatingunit\_lov                                 | null             | lov name identifier                                                 |
| logical                        | dataSourceName |                   | Mandatory          |            |          | DataSource1                                        | <datasourceName/> | datasource attahced to the datamodel                                |
| logical                        | columnNames    |                   | Mandatory          |            |          |                                                    |                  |                                                                     |
| logical                        |                | name              | Mandatory          |            |          | operating\_unit                                    |                  | lov column name                                                     |
| logical                        |                | displayName       | Optional           |            |          | Organization Name                                  | <name/>           | column Label                                                        |
| logical                        |                | visible           | Optional           |            |          | TRUE                                               | FALSE            | flag to check if the field is hidden or not                         |
| logical                        | idProp         |                   | Mandatory          |            |          | org\_id                                            |                  | id value field which will passed to the field on lov select         |
| logical                        | nameProp       |                   | Mandatory          |            |          | operating\_unit                                    |                  | visiable name field which will be passed to the field on lov select |
| logical                        | defaultMapping |                   | Optional           |            |          |                                                    | \[\]             |                                                                     |
| logical                        |                | MappedTo          | Mandatory          |            |          | Create PO.Header.Header Details.bill\_to\_location |                  | mapping field to be set on lov select                               |
| logical                        |                | idProp            | Mandatory          |            |          | bill\_to\_location\_id                             |                  | id value field which will passed to the field on lov select         |
| logical                        |                | nameProp          | Mandatory          |            |          | bill\_to\_location                                 |                  | visiable name field which will be passed to the field on lov select |
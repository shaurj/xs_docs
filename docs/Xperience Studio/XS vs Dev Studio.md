---
sidebar_position: 1
id: Xperience Studio vs Dev Studio
title: Xperience Studio vs Dev Studio
---

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/diffence.mp4" type="video/mp4"/>
  </video>
</figure>

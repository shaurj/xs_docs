---
sidebar_position: 12
---

Alerts are always called from Trigger builtins

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/Alerts.mp4" type="video/mp4"/>
  </video>
</figure>

---
### Properties and Parameters

| Property Type (Logical/Visual) | Name                      | Sub Property Name | Mandatory/Optional | Constraint | ReadOnly | Sample Value           | Default Value          | Description                                                       |
| ------------------------------ | ------------------------- | ----------------- | ------------------ | ---------- | -------- | ---------------------- | ---------------------- | ----------------------------------------------------------------- |
| logical                        | name                      |                   | Mandatory          |            |          | Sample\_Alert          | null                   | alert name identifier                                             |
| logical                        | title                     |                   | Mandatory          |            |          | Item Validation        |                        | title of the alert popup                                          |
| logical                        | message                   |                   | Mandatory          |            |          | maximum value exceeded |                        | alert message                                                     |
| logical                        | alert\_button1            |                   | Mandatory          |            |          | OK                     | Yes                    | button to close the alert                                         |
| visual                         | width                     |                   | Mandatory          |            |          | 450px                  | 300px                  | defines the width of the alertPopup                               |
| visual                         | Height                    |                   | Mandatory          |            |          | 450px                  | 300px                  | defines the height of the alertPopup                              |
| visual                         | x                         |                   | Mandatory          |            |          | 20                     | 0                      | x coordinate of the alert                                         |
| visual                         | y                         |                   | Mandatory          |            |          | 40                     | 0                      | y coordinate of the alert                                         |
| visual                         | theme - background\_color |                   | Optional           |            |          | rgba(255, 255, 255, 1) | rgba(255, 255, 255, 1) | datablock background color                                        |
| visual                         | theme -accent color       |                   | Optional           |            |          | rgba(3, 169, 244, 1)   | rgba(3, 169, 244, 1)   | colors that are used for emphasis in a color scheme               |
| visual                         | theme - text\_color       |                   | Optional           |            |          | rgba(0, 0, 0, 0.87)    | rgba(0, 0, 0, 0.87)    | color of text                                                     |
| visual                         | theme - border\_color     |                   | Optional           |            |          | rgb(255,253,252)       | rgb(255,253,252)       | border color                                                      |
| visual                         | theme - border\_radius    |                   | Optional           |            |          | 4px                    | 4px                    | CSS property rounds the corners of an element's outer border edge |
| visual                         | theme - text\_family      |                   | Optional           |            |          | Roboto Medium          | Roboto Medium          | font text family                                                  |
| visual                         | theme - text\_size        |                   | Optional           |            |          | 12px                   | 12px                   | font text size                                                    |
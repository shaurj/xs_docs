---
sidebar_position: 4
---

Xperience Studio allow users to have parent-child Relation between different components.

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/parent-child.mp4" type="video/mp4"/>
  </video>
</figure>
---
sidebar_position: 7
---

### Basics

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/container.mp4" type="video/mp4"/>
  </video>
</figure>

---
### Creating Tabs with Container

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/containertab.mp4" type="video/mp4"/>
  </video>
</figure>


---
### Properties and Parameters

| Property Type (Logical/Visual) | Name                      | Sub Property Name | Mandatory/Optional | Constraint          | ReadOnly | Sample Value                             | Default Value          | Description                                                                                      | Remarks |
| ------------------------------ | ------------------------- | ----------------- | ------------------ | ------------------- | -------- | ---------------------------------------- | ---------------------- | ------------------------------------------------------------------------------------------------ | ------- |
| logical                        | name                      |                   | Mandatory          |                     |          | home                                     |                        | name identifier of the canvas                                                                    |         |
| logical                        | desc                      |                   | Optional           |                     |          | home                                     |                        | short description of the canvas                                                                  |         |
| logical                        | containerType             |                   |                    |                     |          | Basic/Tab/Panel/Modal                    | Basic                  | defines the container type                                                                       |         |
| logical                        | dynamic                   |                   | Optional           | containerType = Tab |          | TRUE                                     | FALSE                  | is applicable if the canvas is of type tab ,and the user wants to make this tab as a dynamic tab |         |
| logical                        | parentNode                |                   | Mandatory          |                     |          | <parentName, type/>                       |                        | name of parent node instance in the hierarchy                                                    |         |
| logical                        | childNodes                |                   | Optional           |                     | TRUE     | <child1Name , type/> ,<chidl2Name, type/> |                        | list of children nodes of the container                                                          |         |
| visual                         | Label                     |                   | Optional           |                     |          | Landing Page                             | <Container Name/>       | label of the container                                                                           |         |
| visual                         | width                     |                   | Mandatory          |                     |          | 450px                                    | 300px                  | defines the width of the container grid                                                          |         |
| visual                         | Height                    |                   | Mandatory          |                     |          | 450px                                    | 300px                  | defines the Height of the container grid                                                         |         |
| visual                         | x                         |                   | Mandatory          |                     |          | 20                                       | 0                      | x coordinate of the container                                                                    |         |
| visual                         | y                         |                   | Mandatory          |                     |          | 40                                       | 0                      | y coordinate of the container                                                                    |         |
| visual                         | theme - background\_color |                   | Optional           |                     |          | rgba(255, 255, 255, 1)                   | rgba(255, 255, 255, 1) | container background color                                                                       |         |
| visual                         | theme -accent color       |                   | Optional           |                     |          | rgba(3, 169, 244, 1)                     | rgba(3, 169, 244, 1)   | colors that are used for emphasis in a color scheme                                              |         |
| visual                         | theme - text\_color       |                   | Optional           |                     |          | rgba(0, 0, 0, 0.87)                      | rgba(0, 0, 0, 0.87)    | color of text                                                                                    |         |
| visual                         | theme - border\_color     |                   | Optional           |                     |          | rgb(255,253,252)                         | rgb(255,253,252)       | border color                                                                                     |         |
| visual                         | theme - border\_radius    |                   | Optional           |                     |          | 4px                                      | 4px                    | CSS property rounds the corners of an element's outer border edge                                |         |
| visual                         | theme - text\_family      |                   | Optional           |                     |          | Roboto Medium                            | Roboto Medium          | font text family                                                                                 |         |
| visual                         | theme - text\_size        |                   | Optional           |                     |          | 12px                                     | 12px                   | font text size                                                                                   |         |
| visual                         | theme - background-image  |                   |                    |                     |          |                                          |                        |                                                                                                  |         |
| Runtime                        | tabIndex                  |                   | Optional           | containerType = Tab |          | 0                                        | 0                      | defines the tab index  if the container is of type tab                                           |         |
| Runtime                        | visible                   |                   | Optional           |                     |          | TRUE                                     | TRUE                   | iif the container is visible or hidden                                                           |         |
| Runtime                        | Enabled                   |                   | Optional           |                     |          | TRUE                                     | TRUE                   | if the container is enabled or not                                                               |         |
---
sidebar_position: 2
id: Hierarchy
title : Hierarchy and basic functionality
---

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/Heirarchy.mp4" type="video/mp4"/>
  </video>
</figure>
---
sidebar_position: 11
---

### Triggers with builtins - aggregation

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/Triggers with builtins - aggregation.mp4" type="video/mp4"/>
  </video>
</figure>
---

### Triggers with POST API using button

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/Triggers with Buttons.mp4" type="video/mp4"/>
  </video>
</figure>

---
### Properties and parameters

| Property Type (Logical/Visual) | Name | Sub Property Name | Mandatory/Optional | Constraint | ReadOnly | Sample Value                                                                                                                                                                                        | Default Value | Description                                                 |
| ------------------------------ | ---- | ----------------- | ------------------ | ---------- | -------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------- | ----------------------------------------------------------- |
| logical                        |      | name              | Mandatory          |            |          | onClick/onBlur/onFocus/onInit/onChange                                                                                                                                                              | null          | name of the webEvent in which the trigger needs to be fired |
| logical                        |      | \_function        | Mandatory          |            |          | if(builtin.VALUE('Create PO.Header.Header Details.supplier').valueChanges && builtin.GET\_CURRENT\_MODE()=='CREATE' ){builtin.VALUE('Create PO.Header.Header Details.supplier\_site').setValue('')} | null          | trigger function                                            |
| logical                        |      | errorMessage      | Optional           |            |          | Maximum Value exceeded'                                                                                                                                                                             | null          | error message for exception handling                        |
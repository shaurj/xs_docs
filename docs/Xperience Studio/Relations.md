---
sidebar_position: 10
id: Relations
title: Relations
---
Relation can be establised between two or more Data Blocks using a Data Source.

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/Relations.mp4" type="video/mp4"/>
  </video>
</figure>


---
### Properties and Parameters

| Property Type (Logical/Visual) | Name          | Sub Property Name | Mandatory/Optional | ReadOnly | Sample Value | Default Value                                                                                                                  | Description                                    |
| ------------------------------ | ------------- | ----------------- | ------------------ | -------- | ------------ | ------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------- |
| logical                        | name          |                   | Mandatory          |          | Relation1    |                                                                                                                                | relation name identifier                       |
| logical                        | joinCondition |                   | Mandatory          |          |              |  <blockName1/>.<fieldgroupName1/>.<field1/> = <blockName2/>.<fieldgroupName1/>.<field2/>  &  <blockName3/>.<fieldgroupName2/>.<field3/> | join condition which explains the relationship |
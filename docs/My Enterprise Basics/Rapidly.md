---
sidebar_position: 4
id: Rapidly
title: Rapidy
---


### Overview

One stop platform for API Developers. Rapidly helps developers to create, publish and share API's quickly and easily. Built on strong meta data approach for configuring API, Securing your API and sharing it with your team members in minutes.

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/Rapidly.mp4" type="video/mp4"/>
  </video>
</figure>

### Link

<a href="http://193.122.128.117:8002/"target="_blank">http://193.122.128.117:8002/</a>
---
sidebar_position: 3
id: restapi
title: REST API
---

## What is an API ?


API is Application Programming Interface which is used to communicate between two applications or database server to User interface or vice versa.

The following are the most common types of web service APIs:

•	SOAP (Simple Object Access Protocol): This is a protocol that uses XML as a format to transfer data. Its main function is to define the structure of the messages and methods of communication. It also uses WSDL, or Web Services Definition Language, in a machine-readable document to publish a definition of its interface.
SOAP (simple object access protocol) is an established web API protocol. It is intended to be extensible, neutral (able to operate over a range of communication protocols, including HTTP, SMTP, TCP and more), and independent (it allows for any programming style)

•	REST (Representational State Transfer): REST is not a protocol like the other web services, instead, it is a set of architectural principles. The REST service needs to have certain characteristics, including simple interfaces, which are resources identified easily within the request and manipulation of resources using the interface.

•	ORDS provides a Database Management REST API, a web interface for working with database (SQL Developer Web), the ability to create own REST APIs, and a PL/SQL Gateway.

•	ORDS can be deployed on WebLogic or  Tomcat

![image](/img/ords1.jpg)
<br/>

## Standard HTTP Methods

### GET 

•	CRUD Operation : Retrieve
•	Usage: Retrieving a resource

#### POST

•	CRUD Operation :Create
•	Usage: Create a resource at URI

#### PUT

•	CRUD Operation :Update
•	Usage: Updating or Creating a resource at a known URI

#### DELETE

•	CRUD Operation :Delete
•	Usage: Deleting a resource

### Structure of ORDS API:

![image](/img/ords2.png)

<br/>

## How to create an Oracle Rest Data Service in SQL Developer?

**Open the Rest Services view in SQL developer**

![image](/img/ords3.png)

<br/>

**Create a new Module under the Rest Data Services**

![image](/img/ords4.png)

<br/>

**Write a module name you want to make the Rest API for and click Publish Checkbox and click on Next**

![image](/img/ords5.png)

<br/>

**Create a template under the Module Name which will point to the actual description of the requirement and Click on Next.**

**The Restful Service is enabled and Click on Finish.**

![image](/img/ords6.png)

<br/>


**The created module now appears in the list of REST modules in SQL Developer**

![image](/img/ords7.png)

<br/>

At this point, you can create various handlers within this Module-Template.

We have used 2 handlers majorly in the My-Enterprise application-GET and POST.

GET handler will be used when we want to fetch/abstract data from Oracle database by writing a SELECT query with input variables like User Name, OU and Service Order which become the bind variables.

POST handler is used to perform DML operations on the Oracle tables based on the requirement. For inserting or updating data in Oracle base tables, we can call Oracle standard APIs in a custom PL/SQL package.proc which will be attached to this POST handler. We will also define output variables to capture the return message (Success/Error) from the package. proc, value of the O/P to be captured on the UI.

![image](/img/ords8.png)

<br/>

**Walkthrough of REST APIs built for Orders:**

![image](/img/ords9.png)

<br/>

**Orders.fetch_order_headers:**

This will be a GET API as the name itself explains to fetch the Order header information : Order Number,Sold to Customer Details,Order Status, Order Source, Customer PO number, Ordered Date,Order Type, Ship to/Bill to Details, Sales Representative etc.

**The input bind variables are:**

        :p_order_number
        :p_order_status
        :p_order_source
        :p_ou_name

i.e User can search on above fields and get the desired Order Header Information. 

The query has to be written in the GET fetch_order_headers sql editor as shown below. Attaching the query below for your reference.

![image](/img/ords10.png)

<br/>

**The input parameters to be defined in the below manner**

![image](/img/ords11.png)

![image](/img/ords12.png)

<br/>

### Query writing rules for the GET function: 

**Points to be noted:**

1.	The column alias name should be mentioned without “” and should have an _ in the name mandatorily.

2.	We have to fetch the data for the OU to which the user has access. Right now we are showing data for users across all OUs. The logic to enable data access based on user, responsibility and OU is WIP.

3.	Query for Fetching Order header details is attached in the Appendix section.

### POST handler PL/SQL procedure:

**Points to be noted:**

1.	ORDS does not identify PL/SQL Record Type/Table type format logic of PL/SQL for implementing BULK inserts/updates of records in one go. Right now we have implemented design with POST operation on Single row record like Single Order or single order line creation on button click on UI. The discussion to implement Record Format is WIP with ORDS Oracle Product Manager.

2.	The package for all POST operations in Orders module is attached in the Appendix.

3.	Please follow the below screens for understanding how to enable a POST handler API and how the output Parameters are captured.

![image](/img/ords13.png)

![image](/img/ords14.png)

![image](/img/ords15.png)

<br/>

### Testing an API on POSTMAN

Before testing the API on UI, we can test it in POSTMAN to see if desired results are achieved.

For GET API below is the snapshot to see how the RESPONSE Payload is achieved.

![image](/img/ords16.png)

<br/>

**For POST API, We need to prepare a Request Payload in the below format and get the desired result.**

        {	
            "p_user_name":"DEV",
            "p_org_name":"Vision Operations",
                    "p_absence_attendance_reason":"Earned Leave",
            "p_date_start":"07-JAN-2020",
            "p_date_end":"09-JAN-2020"	
        }

![image](/img/ords17.png)

<br/>

****

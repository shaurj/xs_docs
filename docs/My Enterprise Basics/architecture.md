---
sidebar_position: 2
id: architecture
title: Architecture
---

This guide will explain In-depth Architecture of My Enterprise including 
security diagrams, block diagram and other details.

## Overview

![Architecture](/img/architecture.PNG)

1. **System Of Records** : In this layer, we are talking about the data. My Enterprise accepts
REST API as datasource Input. Datasources can be ORDS API, Cloud API. If not API we can generate
API using Spring Boot Microservice. 

2. **Interceptor**: My Enterprise Interceptor Microservice is an optional service which acts as an API Gateways. All the datasources are registered in INterceptor Service. It maintains the catalog of API's and Provide Security, Logging, Monitoring and Discovery.

3. **Keycloak Auth**: My Enterprise Uses Open Source based SSO authentication using Keycloak. Keycloak is implemented as a saperate Microservice which takes care of Social Logins, SSO, User and Access Management. It acts as a centralized Authorization Microservice for My Enterprise Solution Stack.

4. **Developer Studio**: It is a LOW Code IDE for My Enterprise which generates Meta Data JSON which are consumed by My Enterprise Platform. Developer Studio is One Stop solution for Developer to create rapid UI using Drag and Drop Features. Studio enables creation of Data Models, Dashboard jsons and Process Modeler BPMN meta data. 

5. **My Enterprise UI**: My Enterprise UI consists of the following elements
    - Component Interpretor - Process the meta data json in to meaning UI. It interpret and generates UI components as output
    - Admin Screens - It provides role management, user management, application management features. Admin Screens are available only to specific users with Admin access rights.
    - My Profile - View and Manage profile Screen
    - Cognitive Services - It consists of Cognitive Services that My Enterprise provides such as RPA services, Monitoring Services, Chat Bot services etc,.
    - Applications - It shows all the availble applications in My Enterprise along with dashboards inside them.

## Simplified Diagram

![Architecture1](/img/architecture1.PNG)

## Docker Diagram

![Architecture2](/img/architecture2.PNG)

## Infrastructure Security

![Architecture3](/img/architecture3.PNG)
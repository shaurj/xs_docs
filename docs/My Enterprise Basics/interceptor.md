---
sidebar_position: 5
id: interceptor
title: Interceptor
---

## Overview

Interceptor Service is an API Gateway for REST API's. Interceptor Service provides two advantages:
1. **Security**: You can add REST API Security Creds in Interceptor Service Catalog Registry.
Then Interceptor service will take care of all the authorization part and append Authorization headers Automatically
2. **Encapsulation**: You can share only specific API's and not all the available api's in the catalog
Below are the steps for using Interceptor Service:
**Catalog Registeration**- In API Catalog Registeration, Add Host, Path and Authorization details of Instance.
It Also includes adding Modules and API links


![register](/img/register.png)


**APP Management** - In APP Management, Create applications and get the api key.
You can later edit the instances in created application.


![application](/img/application.png)


**Intercept Method** - Intercept is the POST method, using which user can call all the available API's tagged to the mentioned Application.
For instance id Open catalogs Management tab and copy the instance id
You can add query params and body as mentioned below


![usage](/img/usage.png)
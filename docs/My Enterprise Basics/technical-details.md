---
sidebar_position: 7
id: technical-details
title: Technical Details
---

This guide consists of Technical Details - Tools, Technology, Skills required for
My Enterprise Development. 

## Prerequisites
Please find the prerequisites below:

1. HTML5 and CSS - (Good to have SASS knowledge)
2. Angular 8+ 
    - Forms
    - Routing
    - Lazy Loading
    - Data Bindings
    - State Management
    - HTTP module
    - RXJS
3. Responsive Design 
    - Bootstrap 4+
    - CSS Flexbox
4. Component Library - Angular Material
5. NPM or YARN Package Manager

**Good to have knowledge of the following**

1. Nginx App Server
2. Linux commands
3. Docker and Docker Compose

## My Enterprise UI

- Angular 8
- Bootstrap 4
- Angular Material
- AM Charts
- RXJS

## Developer Studio 

- Angular 8
- Bootstrap 4
- Angular Material
- AM Charts
- GrapesJS
- JSON Viewer
- RXJS

## Interceptor UI

- Angular 8
- Bootstrap 4
- Angular Material
- JSON Viewer

## Meta Data Microservice

- Spring Boot 2+
- Keycloak Starter
- MongoDB JPA connector

## Interceptor API

- Spring Boot 2+
- Keycloak Starter
- MongoDB JPA connector
- Rest Template

## Integrator API and Rapidly

- NodeJS
- Express JS
- RXJS
- Axios
- Jason Path
- Swagger 2.0 / open API





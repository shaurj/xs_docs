---
sidebar_position: 6
id: graphdb 
title: Graph Database
---

## My Enterprise –Neo4j User Guide
            
Author         :	Infosys
Creation Date  :	September 14, 2020
Last Updated   :
Version        :	DRAFT 0.1


----
            	
## Table of Contents

1. [What is Graph Database ?](#what)
2. [Why is Graph Database ?](#why)
3. [Introduction to Neo4j.](#intro)
4. [Importing data from oracle to Neo4j.](#import)
5. [Sample use case implementation.](#sample)
6. [Registering APIs to fetch data from Neo4j.](#reg)

<br/>

****

<br/>

## What is Graph Database ? <a name="what"></a>

A graph is a pictorial representation of a set of objects where some pairs of objects are connected by links. It is composed of two elements - nodes (vertices) and relationships (edges).
Graph database is a database used to model the data in the form of graph. In here, each node represents an entity (a person, place, thing, category or other piece of data), and each relationship represents how two nodes are associated. This general-purpose structure allows you to model all kinds of scenarios – from a system of roads, to a network of devices, to a population’s medical history or anything else defined by relationships.

Unlike other databases, relationships take first priority in graph databases. This means your application doesn’t have to infer data connections using things like foreign keys or out-of-band processing, such as MapReduce. The data model for a graph database is also significantly simpler and more expressive than those of relational or other NoSQL databases. Graph databases are built for use with transactional (OLTP) systems and are engineered with transactional integrity and operational availability in mind.

<br/>

**Popular Graph Databases:**

Neo4j is a popular Graph Database. Other Graph Databases are Oracle NoSQL Database, OrientDB, HypherGraphDB, GraphBase, InfiniteGraph, and AllegroGraph.

<br/>

**Our data in Neo4j looks like this:**

![image](/img/1.png)
<br/>
****
<br/>

## Why is Graph Database ? <a name="why"></a>

Today’s CIOs and CTOs don’t just need to manage larger volumes of data – they need to generate insight from their existing data. In this case, the relationships between data points matter more than the individual points themselves. In order to leverage data relationships, organizations need a database technology that stores relationship information as a first-class entity. That technology is a graph database.

Ironically, legacy relational database management systems (RDBMS) are poor at handling data relationships. Their rigid schemas make it difficult to add different connections or adapt to new business requirements.

Not only do graph databases effectively store data relationships; they’re also flexible when expanding a data model or conforming to changing business needs.

Tech giants like Google, Facebook, LinkedIn and PayPal all tapped into the power of graph databases to create booming businesses. Their secret? They each used graph database technology to harness the power of data connections.

A graph database is purpose-built to handle highly connected data, and the increase in the volume and connectedness of today’s data presents a tremendous opportunity for sustained competitive advantage.

### Graph databases have three other key advantages: -
<br/>

**Performance**

For intensive data relationship handling, graph databases improve performance by several orders of magnitude. With traditional databases, relationship queries will come to a grinding halt as the number and depth of relationships increase. In contrast, graph database performance stays constant even as your data grows year over year.

**Flexibility**

With graph databases, IT and data architect teams move at the speed of business because the structure and schema of a graph model flexes as applications and industries change. Rather than exhaustively modeling a domain ahead of time, data teams can add to the existing graph structure without endangering current functionality.

**Agility**

Developing with graph databases aligns perfectly with today’s agile, test-driven development practices, allowing your graph database to evolve in step with the rest of the application and any changing business requirements. Modern graph databases are equipped for frictionless development and graceful systems maintenance.

<br/>

****
<br/>

##  Introduction to Neo4j <a name="intro"></a>

Neo4j is an open-source, NoSQL, native graph database that provides an ACID-compliant transactional backend for your applications. Initial development began in 2003, but it has been publicly available since 2007. The source code, written in Java and Scala, is available for free on GitHub or as a user-friendly desktop application download. Neo4j has both a Community Edition and Enterprise Edition of the database. The Enterprise Edition includes all that Community Edition has to offer, plus extra enterprise requirements such as backups, clustering, and failover abilities.
Neo4j is referred to as a native graph database because it efficiently implements the property graph model down to the storage level. This means that the data is stored exactly as you whiteboard it, and the database uses pointers to navigate and traverse the graph. In contrast to graph processing or in-memory libr/aries, Neo4j also provides full database characteristics, including ACID transaction compliance, cluster support, and runtime failover - making it suitable to use graphs for data in production scenarios.

Some of the following features make Neo4j very popular among developers, architects, and DBAs:-

•	**Cypher**, a declarative query language similar to SQL, but optimized for graphs. Now used by other databases like SAP HANA Graph and Redis graph via the open Cypher project
•	**Constant time traversals** in big graphs for both depth and br/eadth due to efficient representation of nodes and relationships. Enables scale-up to billions of nodes on moderate hardware.
•	**Flexible** property graph schema that can adapt over time, making it possible to materialize and add new relationships later to shortcut and speed up the domain data when the business needs change.
•	**Drivers** for popular programming languages, including Java, JavaScript, .NET, Python, and many more.

### Data Creation and Traversing in neo4j: -

We can create data manually in Neo4j or import from other legacy systems. To perform any operation in Neo4j we will be using cypher query language.

**Create Nodes: -**

organization node: - CREATE (p:Organization { name: 'ABC' }) return p
Defined node has one property i.e. name.

![image](/img/2.png)

Person node: -  CREATE (p:Person { name: 'Jack' }) return p
Defined node has one property i.e. name.

![image](/img/3.png)

<br/>

### **Create Relationship :-**

    MATCH (p:Person),(o:Organization)
    WHERE p.name = 'Jack' AND o.name = 'ABC'
    CREATE (p)-[r:Works_IN]->(o)
    RETURN p,r,o

![image](/img/4.png)

<br/>

### **Query the existing Data using Match: -**

Query employees working in ABC organization

    MATCH (p:Person)-[:Works_IN]->(o:Organization)
    WHERE o.name = 'ABC'
    RETURN p

![image](/img/5.png)

<br/>

****
<br/>

## Importing data from oracle to Neo4j <a name="import"></a>

We can also import data from any legacy system and consume them in Neo4j.

For e.g. we have imported data from Oracle to create nodes.

For this purpose we have used APOC open source libr/ary. We can call the jdbc driver from apoc function using Oracle connection identifier and this will execute the sql passed in the function and will create the nodes based on data fetched from Oracle.

**Create Supplier node**

**Cypher**

![image](/img/6.png)

<br/>

****
<br/>

## Sample use case implementation <a name="sample"></a>

### Use Case :-

Prediction of return – We can predict the probability of return of items based on the historical data from Supplier, by taking the ratio of total quantity of items returned to the supplier and total quantity we received from the supplier.

**Nodes created :-**

1.	Org – Organization information.
2.	OU – Operating unit information
3.	PO – Combined data of PO Headers, PO Lines, PO Line Locations, PO Distributions.
4.	Product – Item information
5.	RCV transaction – RCV transaction data along with shipment header and lines.
6.	Supplier – Supplier data along with site information.

**Relationships created: -**

1.	Supplier sell PO

Cypher :- 

                MATCH (s:Supplier),(p:PO)
                WHERE s.vendor_id = p.vendor_id
                        and s.vendor_site_id = p.vendor_site_id
                CREATE (s)-[r:Sell]->(p)
                RETURN count(r)
           

2.	PO Received RCV transactions

Cypher : - 

                MATCH (p:PO),(rcv:RCV_Trx)
                WHERE rcv.po_line_location_id = p.line_location_id
                    AND rcv.po_line_id          = p.po_line_id
                    AND rcv.po_header_id        = p.po_header_id
                    AND rcv.organization_id     = p.ship_to_organization_id
                CREATE (p)-[r:Received]->(rcv)
                RETURN count(r)

3.	PO Contains product
    
Cypher : -  

               MATCH (p:PO),(prd:Product)
               WHERE  p.item_id = prd.inventory_item_id
                                  and  p.ship_to_organization_id = prd.organization_id
                CREATE (p)-[r:contains]->(prd)                  RETURN count(r)
<br/>


### Use Case Cypher :-

1.	**Cypher for return prediction**


        MATCH (o:OU)
        WHERE toUpper( o.organization_name ) = r1_org
        WITH  o.organization_id AS ou

        MATCH (o:ORG)
        WHERE toUpper( o.organization_code ) = s1_org
        WITH  ou,o.organization_id AS ship_to_org

        MATCH (p:Product)
        WHERE toUpper( p.item_code ) = i1_id
        AND p.organization_id = ship_to_org
        WITH ou,ship_to_org,p.inventory_item_id as product,v1_id AS vendor,s1_id AS vendor_site,dur_1 AS time_gap

        MATCH (s:Supplier)-[:Sell]->(p:PO)-[:Received]->(rcv:RCV_Trx)
        WHERE  toUpper( s.vendor_name ) = vendor
        AND  toUpper( s.supplier_site ) = COALESCE( vendor_site, toUpper( s.supplier_site ) )
        AND  p.item_id = product
        AND  p.org_id = ou
        AND  rcv.transaction_type = trx_typ1
        AND  rcv.organization_id = ship_to_org
        AND  date()- duration({months: toInteger(time_gap)}) <= date( rcv.transaction_date ) < date()
        WITH   ou,ship_to_org,vendor,vendor_site,product,time_gap,sum( toFloat(rcv.quantity)) AS Returned_qty

        MATCH (s:Supplier)-[:Sell]->(p:PO)-[:Received]->(rcv:RCV_Trx)
        WHERE  toUpper( s.vendor_name ) = vendor
        AND  toUpper( s.supplier_site ) = COALESCE( vendor_site, toUpper( s.supplier_site ) )
        AND  p.item_id = product
        AND  p.org_id = ou
        AND  rcv.transaction_type = trx_typ2
        AND  rcv.organization_id = ship_to_org
        AND  date()- duration({months: toInteger(time_gap)}) <= date( rcv.transaction_date ) < date()
        WITH   Returned_qty,sum( toFloat(rcv.quantity)) AS Delivered_qty
        RETURN ( Returned_qty/Delivered_qty )*100  As Return_probability


    Parameter
    r1_org - 'VISION OPERATION'
    s1_org - 'V1' 
    i1_id  - 'F20000'
    v1_id - 'CONSOLIDATED SUPPLIES'
    s1_id	- 'PLANTATION-CTR'
    dur_1 - '300'
    trx_typ1 - 'RETURN TO VENDOR'
    trx_typ2 -  'DELIVER'


![image](/img/7.png)

<br/>

2.	### Like Minded Customer – 

We can detect the like minded customer for a given customer based on the products they are buying. Neo4j comes with some predefined algorithms which can be applied on the data to find out a pattern such as node similarity, path finder , community detection etc. The algorithms can be used by installing the Neo4j gds libr/ary.

![image](/img/8.png)

<br/>

**Nodes created :-**

1.	Customer – hz cust account data
Cypher  
2.	Sales order – sales order header and line data
Cypher 
3.	Product – Item information
Cypher  

<br/>

**Relationship created: -**

1.	Customer purchased sales order

Cypher -        

                MATCH (c:Customer),(s:Sales_Order)
                WHERE c.cust_account_id = s.sold_to_org_id
                MERGE (c)-[r:PURCHASED]->(s)
                RETURN c,r,s

2.	Sales_order orders product

Cypher -        
                
                MATCH (s:Sales_Order),(p:Product)
                WHERE s.inventory_item_id = p.inventory_item_id
                        and s.org_id = p.organization_id
                MERGE (s)-[r:Orders]->(p)
                RETURN s,r,p

**Use case cypher :-**

        MATCH (c1:Customer{customer_name:v1_id})-[r1:PURCHASED]->(s1:Sales_Order)-[r2:Orders]->(p1:Product)
        WITH   c1, collect(distinct id(p1)) AS Items_OF_From_Customer
        MATCH (c2:Customer)-[r3:PURCHASED]->(s2:Sales_Order)-[r4:Orders]->(p2:Product)
        WHERE  c1 <> c2
        WITH   c1, Items_OF_From_Customer, c2, collect(distinct id(p2)) AS Items_OF_To_Customer
        RETURN c1.customer_name AS Customer,
                        c2.customer_name AS Similar_Customer,
                        gds.alpha.similarity.jaccard(Items_OF_From_Customer, Items_OF_To_Customer) AS similarity
        ORDER  BY similarity DESC
        LIMIT  3

**Parameter : -**

        V1_id - 'Imaging Innovations, Inc.' ( Customer name ) 

![image](/img/9.png)

<br/>

****
<br/>

## Registering APIs to fetch data from Neo4j <a name="reg"></a>

We can use cypher queries in APIs and use it in UI components.
Postman is used to get the APIs registered.

### API registration procedure :-

**Method** – POST

**URI** - http://193.122.162.209:3001/integrator/neo4j/api

**Payload** 
```shell
        {
            "name": "like_minded_customer",
            "query": "MATCH (c1:Customer{customer_name:cust_nam})-[r1:PURCHASED]->(s1:Sales_Order)-[r2:Orders]->(p1:Product) WITH   c1, collect(distinct id(p1)) AS Items_OF_From_Customer MATCH (c2:Customer)-[r3:PURCHASED]->(s2:Sales_Order)-[r4:Orders]->(p2:Product) WHERE  c1 <> c2 WITH   c1, Items_OF_From_Customer, c2, collect(distinct id(p2)) AS Items_OF_To_Customer RETURN c1.customer_name AS Customer, c2.customer_name AS Similar_Customer, gds.alpha.similarity.jaccard(Items_OF_From_Customer, Items_OF_To_Customer) AS similarity ORDER  BY similarity DESC LIMIT  3",
            "params":[{"prop":"cust_nam","type":"string"}]
        }
```

![image](/img/10.png)

<br/>

### Calling API via postman :-

**Method** – GET

**URI** - http://193.122.162.209:3001/integrator/neo4j/api/like_minded_customer

**Parameter** –

        cust_nam -> Imaging Innovations, Inc.

![image](/img/11.png)

<br/>

****







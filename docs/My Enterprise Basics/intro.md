---
sidebar_position: 1
title: Introduction
sidebar_label: Introduction
docId: intro
---

My Enterprise: Infosys Enterprise Digital Platform for Oracle economy

## Why My Enterprise?

<figure class="video_container">
  <video width="780" height="480" preload="metadata" controls="true" allowfullscreen="true" >
    <source src="../../vids/Basics of ME.mp4" type="video/mp4"/>
  </video>
</figure>

An Open source stack based solution, services and operating model for our clients to accelerate their digital transformation journey. Platform provides the following:

1. 500+ Ready to consume Enterprise API Catalogues
2. 20+ vertical & horizontal solutions
3. Developer Utility for drag and drop development (IDE), React quickly to business need
4. Auto-Scalable & Containerized with all technology and tools setups
5. Predictive insights through Digital brains & ML solution
6. End to end application landscape Monitoring Solution thereby identifying and reducing critical business interruptions
7. Digital assistance and User interaction through ChatBot Solutions

![Live Enterprise](/img/liveenterprise.PNG)


## Features

1. **Reimagined Experience**: 
    - Modern, global platform there by enhanced user experience
    - Process centric, User experience-focused design
    - any time, anywhere UI, Digital assistant
2. **Connected**: 
    - Unified abstraction layer containing data across all systems.
    - Ready to integrate setups with Oracle applications (ORDS and others)
    - With pre-built components and in-built connectors, “My enterprise” enables your Oracle backend systems to new digital system. 
3. **Agile and Innovative**: 
    - Developer Utility for drag and drop development (IDE)
    - Reduced development life cycle by utilizing Oracle out of box features
    - Micro service, API Driven and Open Source DevOps Solution
    - React quickly to business need

4. **Observable and Evolvable**:
    - monitoring across business processes, applications, infrastructure, databases
    - RPA Solution to cut down operational cost
    - ML based predictive solutions
5. **Lower Risk**:
    - No disruption to source systems yet scalable platform for future expansion
    - A step towards monetizing the Data through “FaaS” model
6. **Lower Cost**:
    - Embracing Open source technology thereby No S/W license cost
    - Cloud native platform hence no vendor  locking
    - reduced TCO by having lower implementation and operational cost.
    - leveraging existing IT investments in ERP systems

## Concept

####  Meta-Data Driven Approach
This development architecture abstracts an entire application into a single metadata file that defines all attributes of the application and executes on a purpose-built delivery platform. While not new, these types of metadata-driven application architectures — mostly in the form of no-code development platforms.

Tight coupling of business and the software implementation is the biggest difficulty of designing software system architecture. The business change will inevitably lead to software restructuring, which is the necessary result of business-driven development, and it greatly restrict the development of software productivity. Metadata driven design, as an architecture pattern, can effectively reduce the coupling relationship between business and software implementation, improve the flexibility of design and improve the reusability of the system. 

Meta Data In My Enterprise:
- Data Model JSON
- Dashboard JSON
- Process Modeler BPMN

Advantages of Meta Data Driven Approach:

1. No Deployment Downtime
2. Rapid Development
3. No Tight Coupling between Business and Software
4. Flexibility of Design

####  REST API

REST is acronym for REpresentational State Transfer. It is architectural style for distributed hypermedia systems and was first presented by Roy Fielding in 2000 in his famous dissertation.

1. Client–server – By separating the user interface concerns from the data storage concerns, we improve the portability of the user interface across multiple platforms and improve scalability by simplifying the server components.

2. Stateless – Each request from client to server must contain all of the information necessary to understand the request, and cannot take advantage of any stored context on the server. Session state is therefore kept entirely on the client.

3. Cacheable – Cache constraints require that the data within a response to a request be implicitly or explicitly labeled as cacheable or non-cacheable. If a response is cacheable, then a client cache is given the right to reuse that response data for later, equivalent requests.

4. Layered system – The layered system style allows an architecture to be composed of hierarchical layers by constraining component behavior such that each component cannot “see” beyond the immediate layer with which they are interacting


| HTTP Method | CRUD | Entire Collection |
| ---------------- | ---------------- | ----------|
| POST     | Create   | 201 (Created) |
| GET    | Read    | 200 (OK), 404 (Not Found) |
| PUT    | Update/Replace    | 405 (Method Not Allowed),200 (OK), 404 (Not Found) |
| PATCH     | Update/Modify   | 405 (Method Not Allowed),200 (OK), 404 (Not Found) |
| DELETE     | Delete    | 200 (OK). 404 (Not Found) |

References

1. [RESTAPI Tutorial](https://www.restapitutorial.com/lessons/httpmethods.html)
2. [https://restfulapi.net/](https://restfulapi.net/)

####  ORDS 

Oracle REST Data Services is a Java EE-based alternative for Oracle HTTP Server and mod_plsql. The Java EE implementation offers increased functionality including a command line based configuration, enhanced security, file caching, and RESTful web services. Oracle REST Data Services also provides increased flexibility by supporting deployments using Oracle WebLogic Server, GlassFish Server, Apache Tomcat, and a standalone mode.

The Oracle Application Express architecture requires some form of web server to proxy requests between a web browser and the Oracle Application Express engine. Oracle REST Data Services satisfies this need but its use goes beyond that of Oracle Application Express configurations. Oracle REST Data Services simplifies the deployment process because there is no Oracle home required, as connectivity is provided using an embedded JDBC driver.

References

1. [ORDS Docs](https://docs.oracle.com/cd/E56351_01/doc.30/e87809/installing-REST-data-services.htm#AELIG7017)

####  SSO Implementation using Keycloak

Single sign-on (SSO) is a property of access control of multiple related, yet independent, software systems.What this means is that we can use a single authentication service to allow users to login to other services, without providing a password to the service that is being logged into.

Keycloak is an open source program that allows you to setup a secure single sign on provider. It supports multiple protocols such as SAML 2.0 and OpenID Connect. It can also store user credentials locally or via an LDAP or Kerberos backend.

![Keycloak Features](/img/keycloak.PNG)

References

1. [Keycloak Official Docs](https://www.keycloak.org/)
2. [SSO Introduction](https://medium.com/@just_insane/keycloak-sso-part-1-what-is-single-sign-on-7229743c289b)
3. [Implementation](https://medium.com/@kamleshbadgujar00/secure-spring-boot-angular-9-application-using-keycloak-1-3-b00e801ba693)

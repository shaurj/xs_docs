/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Home',
  tagline: 'Dinosaurs are cool',
  url: 'https://shaurj.gitlab.io/xs_docs/',
  baseUrl: '/xs_docs/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon-32x32.png',
  organizationName: 'Shaurj', // Usually your GitHub org/user name.
  projectName: 'xs_docs', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Home',
      logo: {
        alt: 'XS Logo',
        src: 'img/favicon-32x32.png',
      },
      items: [
        {
          type: 'doc',
          docId: 'Welcome',
          position: 'left',
          label: 'Documentation',
        },
        
      ],
    },
    footer: {
      style: 'dark',
      copyright: `Copyright © ${new Date().getFullYear()} My Enterprise, Infosys.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          //editUrl:
            //'github repo',
        },
        
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
